package JUnitTests;
import Controller.*;
import Model.*;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import Model.*;
public class KKAddItemTest {
	public static final String debugString = "[DEBUG]: ";
	@BeforeClass
	public static void setUpBeforeClass() throws Exception 
	{
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception 
	{
		
	}

	@Before
	public void setUp() throws Exception 
	{
		
	}

	@After
	public void tearDown() throws Exception 
	{
		
	}

	@Test
	public void addBookToLibraryTest() 
	{
		KKLibrary testLibrary = new KKLibrary();
		String testISBN = "1234";
		String testTitle = "new book";
		System.out.println(debugString + "Testing addBookToLibrary with ISBN: " + testISBN + " and Title: " + testTitle);
		System.out.println(debugString + "Expected value: TRUE");
		boolean resultBoolean = testLibrary.addBookToLibrary(testISBN, testTitle);
		assertTrue(resultBoolean);
		System.out.println(debugString + "Success with value of " + resultBoolean);
		
		System.out.println(debugString + "Testing getNumberOfBooks");
		System.out.println(debugString + "Expected value: 1");
		int resultNumberOfBooks = testLibrary.getNumberOfBooks();
		assertEquals(1, resultNumberOfBooks);
		System.out.println(debugString + "Success with value of " + resultNumberOfBooks);
		
		System.out.println(debugString + "Testing findBookWithISBN with ISBN: " + testISBN);
		System.out.println(debugString + "Expected title: " + testTitle);
		KKBook resultKKBook = testLibrary.findBookWithISBN("1234");
		assertEquals(testISBN, resultKKBook.getISBN());
		assertEquals(testTitle, resultKKBook.getTitle());
		System.out.println(debugString + "Success with ISBN: " + resultKKBook.getISBN() + " and Title: " + resultKKBook.getTitle());
	}
}
