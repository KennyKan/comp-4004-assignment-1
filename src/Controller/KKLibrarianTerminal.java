package Controller;
import java.util.Scanner;

import Model.*;
public class KKLibrarianTerminal {
	private KKLibrary library;
	public KKLibrarianTerminal()
	{
		library = new KKLibrary();
	}
	
	public void findTitle()
	{
		KKBook bookResult = null;
		int inISBN = -1;
		while(inISBN < 0)
		{
			System.out.println("Input ISBN: ");
			Scanner inISBNScanner = new Scanner(System.in);
			while (!inISBNScanner.hasNextInt()) 
			{
	            System.out.println("ERROR: Invalid ISBN input");
	            System.out.println("Input ISBN: ");
	            inISBNScanner.nextLine().replace(" ", "");
	        }
			inISBN = inISBNScanner.nextInt();
		}
		bookResult = this.library.findBookWithISBN(String.valueOf(inISBN));
		if(bookResult == null)
		{
			System.out.println("ERROR: Book not found");
			System.out.println("Please add the book to the library");
			int newISBN = createBook(inISBN);
			if(newISBN >= 0)
				System.out.println("The book with the ISBN: " + newISBN + " has been added");	
		}else{
			System.out.println("======BOOK FOUND======");
			System.out.println("ISBN: " + bookResult.getISBN());
			System.out.println("Title: " + bookResult.getTitle());
			System.out.println("======================");
		}
	}
	
	private int createBook(int inISBN)
	{
		String inTitle = null;
		while (inTitle == null)
		{
			System.out.println("Input title of book: ");
			Scanner inTitleScanner = new Scanner(System.in);
			while (!inTitleScanner.hasNextLine())
			{
				inTitleScanner.nextLine();
			}
			inTitle = inTitleScanner.nextLine();
		}
		boolean success = this.library.addBookToLibrary(String.valueOf(inISBN), inTitle);
		if(success)
			return inISBN;
		else
			return -1;
	}
}
