package Model;

public class KKBook 
{
	private String isbn 	= "";
	private String title 	= "";
	
	public KKBook()
	{
		this.isbn  = "";
		this.title = "";
	}
	
	public void setISBN(String inISBN)
	{
		this.isbn = inISBN;
	}

	public String getISBN() 
	{
		return isbn;
	}

	public String getTitle() 
	{
		return title;
	}

	public void setTitle(String title) 
	{
		this.title = title;
	}
}
