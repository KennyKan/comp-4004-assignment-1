package Model;
import Model.KKBook;

public class KKLibrary 
{
	private final int maxInventory = 1000;
	private int totalNumberOfBooks = 0;
	private KKBook[] bookInventory = new KKBook[maxInventory];
	
	public int getNumberOfBooks()
	{
		return totalNumberOfBooks;
	}
	
	public KKBook getBookAtIndex(int index)
	{
		if(index > getNumberOfBooks()-1)
		{
			System.out.println("ERROR: Index out of bounds");
			return null;
		}
		return bookInventory[index];
	}
	
	public KKBook findBookWithISBN(String inISBN)
	{
		KKBook bookToBeReturned = null;
		int index = 0;
		boolean bookFound = false;
		while (!bookFound && index <= getNumberOfBooks()-1)
		{
			KKBook bookAtIndex = getBookAtIndex(index);
			if(bookAtIndex.getISBN().compareToIgnoreCase(inISBN) == 0)
			{
				bookToBeReturned = bookAtIndex;
				bookFound = true;
			}
		}
		return bookToBeReturned;
	}
	
	public boolean addBookToLibrary(String inISBN, String inTitle)
	{
		KKBook bookToBeAdded = new KKBook();
		bookToBeAdded.setISBN(inISBN);
		bookToBeAdded.setTitle(inTitle);
		
		if(this.totalNumberOfBooks >= maxInventory)
		{
			System.out.println("ERROR: Library is filled to capacity. Please remove a book before adding a new one");
			return false;
		}else{
			bookInventory[totalNumberOfBooks] = bookToBeAdded;
			totalNumberOfBooks++;
			System.out.println("SUCCESS: Book has been added to the library");
			return true;
		}
	}
}
